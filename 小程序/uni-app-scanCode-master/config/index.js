export default {
	req: { baseURL: 'http://127.0.0.1:2140/api' },
	api: {
		login: '/seller/login',
		register: '/seller/register',
		sendDeviceId: '/seller/send_deviceid',
		scanLogin: '/seller/scan_login'
	}
}
