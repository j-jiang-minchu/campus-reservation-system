// 导出一个 mixin 对象
export default {
  computed: {
    path () {
      switch (this.$route.path) {
        case '/main/overview': return ['系统总览']

        case '/main/admin/user': return ['管理系统', '用户管理']

        case '/main/admin/shop': return ['管理系统', '店铺管理']

        case '/main/admin/order': return ['管理系统', '订单管理']

        case '/main/admin/feedback': return ['管理系统', '工单反馈']

        case '/main/shop/register': return ['我的店铺', '注册店铺']

        case '/main/shop/info': return ['我的店铺', '店铺信息']

        case '/main/shop/bill': return ['我的店铺', '店铺流水']

        case '/main/shop/comment': return ['我的店铺', '店铺评价']

        case '/main/shop/comment': return ['我的店铺', '店铺评价']

        case '/main/food/add': return ['食品中心', '新增食品']

        case '/main/food/info': return ['食品中心', '食品信息']

        case '/main/order/today': return ['订单管理', '今日订单']

        case '/main/order/all': return ['订单管理', '全部订单']

        case '/main/profile/myself': return ['个人中心', '我的信息']

        case '/main/profile/feedback': return ['个人中心', '工单反馈']

        case '/main/profile/my_feedback': return ['个人中心', '我的工单']

      }

    },
  }

}
